package v1_test

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/SaidovZohid/excel-reader/api/models"
	"gitlab.com/SaidovZohid/excel-reader/storage/repo"
	"gitlab.com/SaidovZohid/excel-reader/storage/repo/mock_db"
)

func TestGetAllProduct(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	arg := repo.GetAllProductParam{
		Limit: 4,
		Page:  1,
	}

	res := repo.Products{
		Products: []*repo.Product{
			{
				ID:          1,
				Name:        "Pen",
				Sku:         "WE512QWE",
				Description: "lorem ipsum",
				Price:       330.23,
				Count:       1,
				ImageUrl:    "dasdasdasdasdas",
				CreatedAt:   time.Now(),
			},
			{
				ID:          2,
				Name:        "Pencil",
				Sku:         "TY421GHS",
				Description: "lorem ipsum 2",
				Price:       123.23,
				Count:       1,
				ImageUrl:    "dsdasdasdasds",
				CreatedAt:   time.Now(),
			},
			{
				ID:          3,
				Name:        "Album",
				Sku:         "OI098KOP",
				Description: "lorem ipsum 3",
				Price:       130.23,
				Count:       2,
				ImageUrl:    "fdsfdsfdsasdfasdfsdf",
				CreatedAt:   time.Now(),
			},
			{
				ID:          2,
				Name:        "Laptop",
				Sku:         "OU758WKH",
				Description: "lorem ipsum 4",
				Price:       21.23,
				Count:       1,
				ImageUrl:    "dfkhfaeowfhewkljfvbnsv",
				CreatedAt:   time.Now(),
			},
		},
		Count: 18,
	}

	productStrg := mock_db.NewMockProductStorageI(ctrl)
	productStrg.EXPECT().
		GetAll(&arg).Times(1).Return(&res, nil)

	strg.SetProduct(productStrg)

	url := fmt.Sprintf("/v1/products?limit=%d&page=%d", 4, 1)

	req, _ := http.NewRequest(http.MethodGet, url, nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	body, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)

	var response models.GetAllProductResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	assert.Equal(t, res.Count, response.Count)
	assert.Equal(t, len(res.Products), len(response.Products))
}
