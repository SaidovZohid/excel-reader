package v1

import (
	"errors"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/xuri/excelize/v2"
	"gitlab.com/SaidovZohid/excel-reader/api/models"
	"gitlab.com/SaidovZohid/excel-reader/storage/repo"
)

type File struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

// @Router /file-upload [post]
// @Summary File upload
// @Description File upload
// @Tags file-upload
// @Accept json
// @Produce json
// @Param file formData file true "File"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UploadFile(ctx *gin.Context) {
	var file File

	err := ctx.ShouldBind(&file)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	id := uuid.New()
	path := filepath.Ext(file.File.Filename)
	if !checkPath(path) {
		ctx.JSON(http.StatusBadRequest, errorResponse(errors.New("not allowed excel file")))
		return
	}
	fileName := id.String() + filepath.Ext(file.File.Filename)
	dst, _ := os.Getwd()

	if path == ".png" {
		if _, err := os.Stat(dst + "/images"); os.IsNotExist(err) {
			os.Mkdir(dst+"/images", os.ModePerm)
		}
		filePath := "/images/" + fileName
		err = ctx.SaveUploadedFile(file.File, dst+filePath)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
		ctx.JSON(http.StatusOK, models.ResponseOK{
			Message: "https://zohiddev.uz/images/" + fileName,
		})
		return
	}

	if _, err := os.Stat(dst + "/excel-files"); os.IsNotExist(err) {
		os.Mkdir(dst+"/excel-files", os.ModePerm)
	}

	filePath := "/excel-files/" + fileName
	err = ctx.SaveUploadedFile(file.File, dst+filePath)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	f, err := excelize.OpenFile(dst + filePath)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	defer func() {
		if err := f.Close(); err != nil {
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}()

	// Get all the rows in the Sheet1.
	rows, err := f.GetRows("Sheet1")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	if len(rows) == 0 {
		ctx.JSON(http.StatusBadRequest, errorResponse(errors.New("empty file can not add to database")))
	}

	products := repo.Products{
		Products: make([]*repo.Product, 0),
	}

	for _, row := range rows[1:] {
		var p repo.Product
		p.Name = row[0]
		p.Sku = row[1]
		p.Description = row[2]
		p.Price, err = strconv.ParseFloat(row[3], 64)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
		p.ImageUrl = row[4]
		p.Count, err = strconv.ParseInt(row[5], 10, 64)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}

		products.Products = append(products.Products, &p)
	}

	err = h.storage.Product().InsertAll(&products)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseOK{
		Message: "Succesfully uploaded and added to database",
	})
}

// @Router /products [get]
// @Summary Get products
// @Description Get products
// @Tags file-upload
// @Accept json
// @Produce json
// @Param params query models.GetAllProductParams false "Params"
// @Success 200 {object} models.GetAllProductResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllProduct(ctx *gin.Context) {
	params, err := validateGetAllParams(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	result, err := h.storage.Product().GetAll(&repo.GetAllProductParam{
		Page:   params.Page,
		Limit:  params.Limit,
		Search: params.Search,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, getProductsResponse(result))
}

func getProductsResponse(data *repo.Products) *models.GetAllProductResponse {
	response := models.GetAllProductResponse{
		Products: make([]*models.Product, 0),
		Count:    data.Count,
	}

	for _, product := range data.Products {
		u := parseProductModel(product)
		response.Products = append(response.Products, &u)
	}

	return &response
}

func parseProductModel(product *repo.Product) models.Product {
	return models.Product{
		ID:          product.ID,
		Name:        product.Name,
		Sku:         product.Sku,
		Description: product.Description,
		Price:       product.Price,
		Count:       product.Count,
		ImageUrl:    product.ImageUrl,
		CreatedAt:   product.CreatedAt,
	}
}
