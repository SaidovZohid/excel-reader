package v1_test

import (
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/SaidovZohid/excel-reader/api"
	"gitlab.com/SaidovZohid/excel-reader/config"
	"gitlab.com/SaidovZohid/excel-reader/storage"
)

var (
	router *gin.Engine
	strg   storage.StorageI
)

func TestMain(m *testing.M) {
	cfg := config.Load("./../..")
	connStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Open("postgres", connStr)
	if err != nil {
		log.Fatalf("failed to open connection: %v", err)
	}

	strg = storage.NewStoragePg(psqlConn)

	ginEngine := api.New(&api.RouterOptions{
		Cfg:     &cfg,
		Storage: strg,
	})

	router = ginEngine

	os.Exit(m.Run())
}
