CREATE TABLE IF NOT EXISTS "products" (
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(100) NOT NULL,
    "sku" VARCHAR(50) NOT NULL,
    "description" TEXT NOT NULL,
    "price" NUMERIC(18, 2) NOT NULL,
    "count" INTEGER DEFAULT 1,
    "image_url" text not null,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)