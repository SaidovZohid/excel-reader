package main

import (
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/SaidovZohid/excel-reader/api"
	"gitlab.com/SaidovZohid/excel-reader/config"
	"gitlab.com/SaidovZohid/excel-reader/storage"

	_ "gitlab.com/SaidovZohid/excel-reader/api/docs" // for swagger
)

func main() {
	cfg := config.Load(".")
	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	strg := storage.NewStoragePg(psqlConn)

	apiServer := api.New(&api.RouterOptions{
		Cfg:     &cfg,
		Storage: strg,
	})

	if err = apiServer.Run(cfg.HttpPort); err != nil {
		log.Fatalf("failed to run server: %v", err)
	}

}
