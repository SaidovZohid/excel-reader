package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/SaidovZohid/excel-reader/storage/repo"
)

func insertProducts(t *testing.T) {
	arg := repo.Products{
		Products: []*repo.Product{
			{
				Name: "Pen",
				Sku: "WE512QWE",
				Description: "lorem ipsum",
				Price: 330.23,
				Count: 12,
				ImageUrl: "fsdfhkjahsdfkdshfas",
			},
			{
				Name: "Pencil",
				Sku: "TY421GHS",
				Description: "lorem ipsum 2",
				Price: 123.23,
				Count: 1,
				ImageUrl: "fksdfhdsafhdsfdsfadk",
			},
			{
				Name: "Album",
				Sku: "OI098KOP",
				Description: "lorem ipsum 3",
				Price: 130.23,
				Count: 3,
				ImageUrl: "fksdfhdsafhdsfdsfadk",
			},
			{
				Name: "Laptop",
				Sku: "OU758WKH",
				Description: "lorem ipsum 4",
				Price: 21.23,
				Count: 5,
				ImageUrl: "fksdfhdsafhdsfdsfadk",
			},
		},
	}
	err := strg.Product().InsertAll(&arg)
	require.NoError(t, err)
}

func TestInsertProducts(t *testing.T) {
	insertProducts(t)
}

func TestGetAll(t *testing.T) {
	insertProducts(t)
	products, err := strg.Product().GetAll(&repo.GetAllProductParam{
		Limit: 4,
		Page:  1,
	})
	require.NoError(t, err)

	require.NotEmpty(t, products)
	require.Equal(t, 4, len(products.Products))
}