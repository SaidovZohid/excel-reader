package postgres

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/SaidovZohid/excel-reader/storage/repo"
)

type productRepo struct {
	db *sqlx.DB
}

func NewProduct(db *sqlx.DB) repo.ProductStorageI {
	return &productRepo{
		db: db,
	}
}

func (pd *productRepo) GetBySku(skuNumber string) (int64, error) {
	query := `
		SELECT id FROM products WHERE sku = $1
	`
	var id int64
	err := pd.db.QueryRow(
		query,
		skuNumber,
	).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (pd *productRepo) InsertAll(p *repo.Products) error {
	queryInsert := `
		INSERT INTO products (
			name,
			sku,
			description,
			price,
			count,
			image_url
		) values ($1,$2,$3,$4,$5,$6)
	`

	queryUpdate := `
		UPDATE products SET 
			name = $1,
			description = $2,
			price = $3,
			count = count + $4,
			image_url = $5
		WHERE id = $6
	`

	for _, product := range p.Products {
		id, err := pd.GetBySku(product.Sku)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				r, err := pd.db.Exec(
					queryInsert,
					product.Name,
					product.Sku,
					product.Description,
					product.Price,
					product.Count,
					product.ImageUrl,
				)
				if res, _ := r.RowsAffected(); res == 0 {
					return err
				}
				continue
			}
			return err
		}
		r, err := pd.db.Exec(
			queryUpdate,
			product.Name,
			product.Description,
			product.Price,
			product.Count,
			product.ImageUrl,
			id,
		)
		if res, _ := r.RowsAffected(); res == 0 {
			return err
		}
	}

	return nil
}

func (pd *productRepo) GetAll(params *repo.GetAllProductParam) (*repo.Products, error) {
	result := repo.Products{
		Products: make([]*repo.Product, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		str := "%" + params.Search + "%"
		filter = fmt.Sprintf(`
			WHERE name ilike '%s' OR sku ilike '%s' 
		`, str, str)
	}

	query := `
		SELECT 
			id,
			name,
			sku,
			description,
			price,
			count,
			image_url,
			created_at
		FROM products 
	` + filter + `
		ORDER BY created_at desc
	` + limit
	rows, err := pd.db.Query(
		query,
	)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var p repo.Product
		err := rows.Scan(
			&p.ID,
			&p.Name,
			&p.Sku,
			&p.Description,
			&p.Price,
			&p.Count,
			&p.ImageUrl,
			&p.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		result.Products = append(result.Products, &p)
	}
	queryCount := "SELECT count(1) FROM products " + filter
	if err = pd.db.QueryRow(queryCount).Scan(&result.Count); err != nil {
		return nil, err
	}
	return &result, nil
}
