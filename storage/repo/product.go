package repo

import "time"

//go:generate mockgen -source ./product.go -package mock_db -destination ./mock_db/mock_product.gen.go
type ProductStorageI interface {
	InsertAll(p *Products) error
	GetAll(param *GetAllProductParam) (*Products, error)
}

type Product struct {
	ID          int64
	Name        string
	Sku         string
	Description string
	Price       float64
	Count       int64
	ImageUrl    string
	CreatedAt   time.Time
}

type Products struct {
	Products []*Product
	Count    int64
}

type GetAllProductParam struct {
	Limit  int64
	Page   int64
	Search string
}
