package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/SaidovZohid/excel-reader/storage/postgres"
	"gitlab.com/SaidovZohid/excel-reader/storage/repo"
)

type StorageI interface {
	Product() repo.ProductStorageI
	SetProduct(p repo.ProductStorageI)
}

type storagePg struct {
	productRepo repo.ProductStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		productRepo: postgres.NewProduct(db),
	}
}

func (s *storagePg) Product() repo.ProductStorageI {
	return s.productRepo
}
